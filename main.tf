resource "oci_core_volume_backup_policy" "this" {
  compartment_id     = var.compartment_id
  defined_tags       = var.defined_tags
  destination_region = var.destination_region
  display_name       = var.display_name
  freeform_tags      = var.freeform_tags

  dynamic "schedules" {
    for_each = var.schedules
    content {
      backup_type       = lookup(schedules.value, "backup_type", local.defaults.schedules.backup_type)
      period            = lookup(schedules.value, "period", local.defaults.schedules.period)
      retention_seconds = lookup(schedules.value, "retention_seconds", local.defaults.schedules.retention_seconds)
      day_of_month      = lookup(schedules.value, "day_of_month", local.defaults.schedules.day_of_month)
      day_of_week       = lookup(schedules.value, "day_of_week", local.defaults.schedules.day_of_week)
      hour_of_day       = lookup(schedules.value, "hour_of_day", local.defaults.schedules.hour_of_day)
      month             = lookup(schedules.value, "month", local.defaults.schedules.month)
      offset_seconds    = lookup(schedules.value, "offset_seconds", local.defaults.schedules.offset_seconds)
      offset_type       = lookup(schedules.value, "offset_type", local.defaults.schedules.offset_type)
      time_zone         = lookup(schedules.value, "time_zone", local.defaults.schedules.time_zone)
    }
  }
}
