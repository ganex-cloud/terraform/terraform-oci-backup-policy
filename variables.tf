variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the compartment."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "display_name" {
  description = "(Optional) (Updatable) A user-friendly name for the volume backup policy. Does not have to be unique and it's changeable"
  type        = string
  default     = ""
}

variable "destination_region" {
  description = "(Optional) (Updatable) The paired destination region (pre-defined by oracle) for scheduled cross region backup calls. "
  type        = string
  default     = null
}

variable "schedules" {
  ## https://www.terraform.io/docs/providers/oci/r/core_volume_backup_policy.html#schedules
  description = "(Optional) (Updatable) The collection of schedules for the volume backup policy."
  type        = list(map(string))
  default     = []
}
