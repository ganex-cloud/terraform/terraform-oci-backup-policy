locals {
  defaults = {
    schedules = {
      backup_type       = "INCREMENTAL"
      period            = "ONE_DAY"
      retention_seconds = 86400
      day_of_month      = 1
      day_of_week       = "MONDAY"
      hour_of_day       = 0
      month             = "JANUARY"
      offset_seconds    = 0
      offset_type       = "STRUCTURED"
      time_zone         = "UTC"
    }
  }
}
