module "backup_policy-default" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-backup-policy.git?ref=master"
  compartment_id = module.compartiment.compartment_id
  display_name   = "default"
  schedules = [
    {
      # Daily incremental
      backup_type       = "INCREMENTAL"
      period            = "ONE_DAY"
      hour_of_day       = 03
      retention_seconds = 86400
    },
    {
      # Weekly full
      backup_type       = "FULL"
      period            = "ONE_WEEK"
      day_of_week       = "SUNDAY"
      hour_of_day       = 02
      retention_seconds = 2592000
    }
  ]
}
